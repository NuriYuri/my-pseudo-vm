# Separating module (to prevent namespace confusion)
module NuriYuri
  # "Virtual Machine" module able to execute its own instruction set
  module VM
    # Compiler class used to create a program with pseudo assembly instruction (passed to #compile)
    #
    # The program executed by the VM consist of two stack :
    #   - The program stack (opcode)
    #   - The value stack (opcode arguments)
    # It allows easy interpretation
    class Compiler
      # Create a new compiler instance with an empty program_stack and an empty value stack
      def initialize
        @program_stack = []
        @value_stack = []
      end

      # Alias instance_eval to compile in order use Ruby as an "Assembly" language
      alias compile instance_eval

      # Dump the program to file or string
      # @param f [IO, nil] the output file
      def dump(f = nil)
        return Marshal.dump([@program_stack, @value_stack], f) if f
        Marshal.dump([@program_stack, @value_stack])
      end

      # Return the program and value stack
      def get_program
        return @program_stack, @value_stack
      end

      # Store an opcode and a value to their stack
      # @param id [Integer] opcode id
      # @param val [Object] value for the opcode
      def exec(id, val)
        @program_stack << id
        @value_stack << val
      end

      # Call a Ruby method
      # @param symbol [Symbol]
      def call_method(symbol)
        exec 0, symbol.to_sym
      end

      # Reset the local variable stack (usefull to switch context between calls)
      # @param size [Integer] the size of the local variable stack
      def reset_local_stack(size)
        exec 1, size.to_i
      end

      # Load the "self" special variable
      # @note The self variable is the current_object when a method was called, it's an Object.new when the code is still in the VM#run context.
      def load_self(*)
        exec 2, nil
      end

      # Set the current object to obj
      # @param obj [Object]
      def set_object(obj)
        exec 3, obj
      end

      # Store the current object to a local variable
      # @param n [Integer] index of the local variable
      def store_to_local(n)
        exec 4, n.to_i
      end

      # Load a local variable to current object
      # @param n [Integer] index of the local variable
      def load_local(n)
        exec 5, n.to_i
      end

      # Push the current object to the arguments to send and load a local variable to it
      # @param n [Integer] index of the local variable
      def push_object_to_arg_load_local(n)
        exec 6, n.to_i
      end

      # Push the current object to the local variable stack and load a local variable to it
      # @param n [Integer] index of the local variable
      def push_object_load_local(n)
        exec 7, n.to_i
      end

      # Push a local variable to the arguments to send
      # @param n [Integer] index of the local variable
      def push_local_to_arg(n)
        exec 8, n.to_i
      end

      # Push a value to the arguments to send
      # @param value [Object]
      def push_value_to_arg(value)
        exec 9, value
      end

      # Push a value to the local variable stack
      # @param value [Object]
      def push_value(value)
        exec 10, value
      end

      # Set the current object to a kwarg
      # @param symbol [Symbol] the name of the kwarg
      def set_object_to_kwarg(symbol)
        exec 11, symbol.to_sym
      end

      # Jump by n instructions if current object
      # @param n [Integer]
      def jump_if_add(n)
        exec 12, n.to_i
      end

      # Jump by n instruction unless current object
      # @param n [Integer]
      def jump_unless_add(n)
        exec 13, n.to_i
      end

      # Jump by the value of the local variable n if current object
      # @param n [Integer]
      def jump_if_add_local(n)
        exec 14, n.to_i
      end

      # Jump by the value of the local variable n unless current object
      def jump_unless_add_local(n)
        exec 15, n.to_i
      end

      # Jump to pc if current object
      # @param pc [Integer] the target program counter
      def jump_to_if(pc)
        exec 16, pc.to_i
      end

      # Jump to pc unless current object
      # @param pc [Integer] the target program counter
      def jump_to_unless(pc)
        exec 17, pc.to_i
      end

      # Change the program counter to the value of the local variable n if current object
      # @param n [Integer]
      def jump_to_if_local(n)
        exec 18, n.to_i
      end

      # Change the program counter to the value of the local variable n unless current object
      # @param n [Integer]
      def jump_to_unless_local(n)
        exec 19, n.to_i
      end

      # Load the received argument n to current object
      # @param n [Integer]
      def load_arg(n)
        exec 20, n.to_i
      end

      # Load the received kwarg to current object
      # @param symbol [Symbol]
      def load_kwarg(symbol)
        exec 21, symbol.to_sym
      end

      # Perform a call operation by adding n to the program counter
      # @param n [Integer]
      def call_add(n)
        exec 22, n.to_i
      end

      # Perform a call operation by adding the local variable n to the program counter
      # @param n [Integer]
      def call_add_local(n)
        exec 23, n.to_i
      end

      # Perform a call operation by changing the program counter to pc
      # @param pc [Integer] the new program counter value
      def call_static(pc)
        exec 24, pc.to_i
      end

      # Perform a call operation by changing the program counter to the value of a local variable
      # @param n [Integer]
      def call_static_local(n)
        exec 25, n.to_i
      end

      # Return from the call operation and set value to current_object
      # @param value [Object]
      def return_value(value)
        exec 26, value
      end

      # Return from the call operation and set the local variable n to current_object
      # @param n [Integer]
      def return_local(n)
        exec 27, n.to_i
      end

      # Jump by adding n to the program counter
      # @param n [Integer]
      def jump_to_add(n)
        exec 28, n.to_i
      end

      # Change the program counter to pc
      # @param pc [Integer]
      def jump_to(pc)
        exec 29, pc.to_i
      end

      # Jump by adding the value of the local variable n to the program counter
      # @param n [Integer] the index of the local variable
      def jump_to_add_local(n)
        exec 30, n.to_i
      end
      
      # Change the procgram counter to the value of the local variable n
      # @param n [Integer] the index of the local variable
      def jump_to_local(n)
        exec 31, n.to_i
      end
    end

    module_function

    # Run a program
    # @param program_stack [Array<Integer>] the opcode stack
    # @param values_stack [Array] the values associated to the opcode
    # @param received_args [Array] the arguments to send to the program
    # @param received_kwarg [Hash] the kwarg to send to the program
    # @note The function is programmed to have fast access to the contents so :v
    def run(program_stack, values_stack, *received_args, **received_kwarg)
      _program_size = program_stack.size
      program_counter = 0
      current_object = Object.new
      local_stack = []
      sent_args = []
      sent_kwarg = {}

      args_stack = []
      kwarg_stack = []

      local_stack_stack = []
      _self = current_object
      _self_stack = []

      _operations = []
      _operations[0] = # call_method symbol
        lambda do
          #print([current_object, values_stack[program_counter], *sent_args, sent_kwarg].join(', '))
          if sent_kwarg.size > 0
            current_object = current_object.public_send(values_stack[program_counter], *sent_args, **sent_kwarg)
          else
            current_object = current_object.public_send(values_stack[program_counter], *sent_args)
          end
          sent_args.clear
          sent_kwarg.clear
          #puts " => #{current_object}"
        end
      _operations[1] = # reset_local_stack
        lambda { local_stack = Array.new(values_stack[program_counter]) }
      _operations[2] = # load_self
        lambda { current_object = _self }
      _operations[3] = # set_object
        lambda { current_object = values_stack[program_counter] }
      _operations[4] = # store_to_local
        lambda { local_stack[values_stack[program_counter]] = current_object }
      _operations[5] = # load_local
        lambda { current_object = local_stack[values_stack[program_counter]] }
      _operations[6] = # push_object_to_arg_load_local
        lambda do
          sent_args << current_object
          current_object = local_stack[values_stack[program_counter]]
        end
      _operations[7] = # push_object_load_local
        lambda do
          local_stack << current_object
          current_object = local_stack[values_stack[program_counter]]
        end
      _operations[8] = # push_local_to_arg
        lambda { sent_args << local_stack[values_stack[program_counter]] }
      _operations[9] = # push_value_to_arg
        lambda { sent_args << values_stack[program_counter] }
      _operations[10] = # push_value
        lambda { local_stack << (current_object = values_stack[program_counter]) }
      _operations[11] = # set_object_to_kwarg
        lambda { sent_kwarg[values_stack[program_counter]] = current_object }
      _local_jump_test = lambda do
        if program_counter >= _program_size || program_counter < 0
          raise LocalJumpError, "Jumped outside program"
        end
        program_counter -= 1 # Adjustement for the program_counter manager
      end
      _operations[12] = # jump_if_add
        lambda do
          if current_object
            program_counter += values_stack[program_counter] 
            _local_jump_test.call
          end
        end
      _operations[13] = # jump_unless_add
        lambda do
          unless current_object
            program_counter += values_stack[program_counter]
            _local_jump_test.call
          end
        end
      _operations[14] = # jump_if_add_local
        lambda do
          if current_object
            program_counter += local_stack[values_stack[program_counter]]
            _local_jump_test.call
          end
        end
      _operations[15] = # jump_unless_add_local
        lambda do
          unless current_object
            program_counter += local_stack[values_stack[program_counter]]
            _local_jump_test.call
          end
        end
      _operations[16] = # jump_to_if
        lambda do
          if current_object
            program_counter = values_stack[program_counter]
            _local_jump_test.call
          end
        end
      _operations[17] = # jump_to_unless
        lambda do
          unless current_object
            program_counter = values_stack[program_counter]
            _local_jump_test.call
          end
        end
      _operations[18] = # jump_to_if_local
        lambda do
          if current_object
            program_counter = local_stack[values_stack[program_counter]]
            _local_jump_test.call
          end
        end
      _operations[19] = # jump_to_unless_local
        lambda do
          unless current_object
            program_counter = local_stack[values_stack[program_counter]]
            _local_jump_test.call
          end
        end
      _operations[20] = # load_arg
        lambda { current_object = received_args[values_stack[program_counter]] }
      _operations[21] = # load_kwarg
        lambda { current_object = received_kwarg[values_stack[program_counter]] }
      _call_store = lambda do
        local_stack << program_counter
        local_stack_stack << local_stack
        args_stack << received_args
        kwarg_stack << received_kwarg
        _self_stack << _self
        _self = current_object
        received_args = sent_args
        sent_args = []
        received_kwarg = sent_kwarg
        sent_kwarg = []
      end
      _operations[22] = # call_add
        lambda do
          _call_store.call
          program_counter += values_stack[program_counter]
          _local_jump_test.call
        end
      _operations[23] = # call_add_local
        lambda do
          _call_store.call
          program_counter += local_stack[values_stack[program_counter]]
          _local_jump_test.call
        end
      _operations[24] = # call_static
        lambda do
          _call_store.call
          program_counter = values_stack[program_counter]
          _local_jump_test.call
        end
      _operations[25] = # call_static_local
        lambda do
          _call_store.call
          program_counter = local_stack[values_stack[program_counter]]
          _local_jump_test.call
        end
      _return_pop = proc do
        local_stack = local_stack_stack.pop
        return current_object if local_stack.nil?
        program_counter = local_stack.pop
        sent_args = []
        sent_kwarg = []
        received_args = args_stack.pop
        received_kwarg = kwarg_stack.pop
        _self = _self_stack.pop
      end
      _operations[26] = # return_value
        lambda do
          current_object = values_stack[program_counter]
          _return_pop.call
        end
      _operations[27] = # return_local
        lambda do
          current_object = local_stack[values_stack[program_counter]]
          _return_pop.call
        end
      _operations[28] = # jump_to_add
        lambda do
          program_counter += values_stack[program_counter]
          _local_jump_test.call
        end
      _operations[29] = # jump_to
        lambda do
          program_counter = values_stack[program_counter]
          _local_jump_test.call
        end
      _operations[30] = # jump_to_add_local
        lambda do
          program_counter += local_stack[values_stack[program_counter]]
          _local_jump_test.call
        end
      _operations[31] = # jump_to_add_local
        lambda do
          program_counter = local_stack[values_stack[program_counter]]
          _local_jump_test.call
        end
      
      # Actual process
      while program_counter < _program_size
        _operations[program_stack[program_counter]].call
        program_counter += 1
      end

      return current_object
    end
  end
end

c = NuriYuri::VM::Compiler.new
c.compile {
  push_value 5
  push_value 6 # current_object becomes 6
  push_value_to_arg 5 # current object doesn't change on that instr
  call_method :* # b * 5
  push_object_to_arg_load_local 0
  call_method :+ # b * 5 + a
  store_to_local 2
}
p NuriYuri::VM.run(*c.get_program)

factoriel = NuriYuri::VM::Compiler.new
# def factoriel(n)
# return 1 if n <= 1
# return factoriel(n - 1) * n
factoriel.compile {
  reset_local_stack 0 # new local stack
  push_value_to_arg 1
  load_arg 0
  call_method :<=
  jump_unless_add 2 # jump after recursive call
  return_value 1
  push_value_to_arg 1
  load_arg 0
  store_to_local 0
  call_method :-
  push_object_to_arg_load_local 0
  call_static 0
  push_object_to_arg_load_local 0
  call_method :*
  push_object_load_local 1
  return_local 1
}
p NuriYuri::VM.run(*factoriel.get_program, 1)
p NuriYuri::VM.run(*factoriel.get_program, 2)
p NuriYuri::VM.run(*factoriel.get_program, 3)
p NuriYuri::VM.run(*factoriel.get_program, 4)
p NuriYuri::VM.run(*factoriel.get_program, 5)
p NuriYuri::VM.run(*factoriel.get_program, 6)

factoriel2 = NuriYuri::VM::Compiler.new
# def factoriel2(n)
# res = 1
# for i = n; i > 1; i--
#   res *= i
# return res
factoriel2.compile {
  reset_local_stack 0 # I use push value no preinit
  push_value 1 # res = 1
  load_arg 0
  store_to_local 1 # i = n
  push_value_to_arg 1
  call_method :>
  jump_unless_add 10
  push_local_to_arg 1
  load_local 0
  call_method :* # res * i
  store_to_local 0 # res *= i
  push_value_to_arg 1
  load_local 1
  call_method :- # i - 1
  store_to_local 1 # i -= 1
  jump_to_add -11
  load_local 0
}

t = Time.new
a = NuriYuri::VM.run(*factoriel2.get_program, 10000)
t = Time.new - t
p t

def factoriel3(n)
  res = 1
  n.downto(2) { |i| res *= i }
  res
end

t = Time.new
b = factoriel3(10000)
t = Time.new - t
p t
p a == b

p factoriel.dump
p factoriel2.dump

p NuriYuri::VM.run(*Marshal.load(factoriel2.dump), 10000) == b


test = NuriYuri::VM::Compiler.new
test.compile {
  load_kwarg :a
  call_method :inspect
  store_to_local 0
  load_kwarg :b
  call_method :inspect
  set_object_to_kwarg :arg2
  load_local 0
  set_object_to_kwarg :arg1
  push_value_to_arg "a = %<arg1>s\nb = %<arg2>s\n---"
  set_object Kernel
  call_method :format
  push_object_to_arg_load_local 0
  set_object Kernel
  call_method :puts
}

NuriYuri::VM.run(*test.get_program, a: 0, b: 1)
NuriYuri::VM.run(*test.get_program, a: 5, b: "str")
NuriYuri::VM.run(*test.get_program, b: 1)